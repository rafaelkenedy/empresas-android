package com.rafael.testioasys.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.rafael.testioasys.databinding.ActivitySplashScreenBinding
import com.rafael.testioasys.R

import com.rafael.testioasys.extensions.setTranslucentWindowControls
import com.rafael.testioasys.extensions.setupFullScreenSystemUiFlags
import com.rafael.testioasys.ui.main.MainActivity
import com.rafael.testioasys.ui.signin.SignInActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class SplashScreenActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivitySplashScreenBinding

    private val viewModel: SplashScreenViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        setupFullScreenSystemUiFlags()
        setTranslucentWindowControls(
                navigationBarColor = ContextCompat.getColor(
                        baseContext,
                        R.color.colorDefaultNavigationBar
                ), withLightStatusBar = true, withLightNavigationBar = true
        )

        viewBinding.root.postDelayed({
            startApp()
        }, START_DELAY)
    }

    private fun startApp() {
        viewModel.checkCurrentAuthentication(::onSuccessAuth, ::onFailureAuth)
    }

    private fun onSuccessAuth() {
        startScreen(MainActivity::class.java)
    }

    private fun onFailureAuth() {
        //startScreen(MainActivity::class.java)
        startScreen(SignInActivity::class.java)
    }

    private fun <T> startScreen(className: Class<T>) {
        startActivity(Intent(this, className))
    }

    companion object {
        private const val START_DELAY = 1500L
    }
}