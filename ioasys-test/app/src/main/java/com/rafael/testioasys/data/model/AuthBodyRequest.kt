package com.rafael.testioasys.data.model

data class AuthBodyRequest(val email: String, val password: String)
