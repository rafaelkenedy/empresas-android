package com.rafael.testioasys.di
import com.rafael.testioasys.data.api.auth.UserAuthAPI
import com.rafael.testioasys.data.api.enterprise.EnterpriseAPI
import com.rafael.testioasys.data.auth.UserAuth
import com.rafael.testioasys.data.dao.EnterpriseDAO
import com.rafael.testioasys.data.dao.UserAuthDAO
import com.rafael.testioasys.data.repository.*
import com.rafael.testioasys.data.repository.auth.*
import com.rafael.testioasys.data.repository.enterprise.EnterpriseProvider
import com.rafael.testioasys.data.repository.enterprise.EnterpriseRepository
import com.rafael.testioasys.data.repository.enterprise.EnterprisesRepository
import com.rafael.testioasys.data.repository.enterprise.RemoteEnterpriseProvider
import com.rafael.testioasys.data.sharedpreferences.UserSharedPreferences
import com.rafael.testioasys.ui.enterprisedetails.EnterpriseDetailsViewModel
import com.rafael.testioasys.ui.search.SearchViewModel
import com.rafael.testioasys.ui.signin.SignInViewModel
import com.rafael.testioasys.ui.splashscreen.SplashScreenViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    single<UserAuthDAO> { UserSharedPreferences(androidContext()) }
    single<UserAuth> { UserAuthAPI() }
    single<EnterpriseDAO> { EnterpriseAPI(get()) }
    single<LocalAuthProvider> { LocalUserAuth(get()) }
    single<RemoteAuthProvider> { RemoteUserAuth(get()) }
    single<RemoteEnterpriseProvider> { EnterpriseProvider(get()) }
    single<AuthRepository> { UserAuthRepository(get(), get()) }
    single<EnterpriseRepository> { EnterprisesRepository(get()) }
}

val splashModule = module {
    viewModel { SplashScreenViewModel(get()) }
}

val signInModule = module {
    viewModel { SignInViewModel(get()) }
}

val searchModule = module {
    viewModel { SearchViewModel(get()) }

}

val enterpriseDetailsModule = module {
    viewModel { EnterpriseDetailsViewModel() }
}
