package com.rafael.testioasys.data.repository.auth

import com.rafael.testioasys.data.auth.UserAuth
import com.rafael.testioasys.data.model.UserAuthData

class RemoteUserAuth(private val userAuth: UserAuth): RemoteAuthProvider {

    override suspend fun auth(email: String, password: String): UserAuthData = userAuth.doAuth(email, password)
}