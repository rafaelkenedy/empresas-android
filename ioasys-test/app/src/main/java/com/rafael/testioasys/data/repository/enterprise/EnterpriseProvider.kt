package com.rafael.testioasys.data.repository.enterprise

import com.rafael.testioasys.data.dao.EnterpriseDAO
import com.rafael.testioasys.data.model.Enterprise

class EnterpriseProvider(private val enterpriseDAO: EnterpriseDAO) : RemoteEnterpriseProvider {

    override suspend fun search(query: String): List<Enterprise> = enterpriseDAO.search(query)
}