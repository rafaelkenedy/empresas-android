package com.rafael.testioasys.ui.enterprisedetails

import androidx.lifecycle.ViewModel
import com.rafael.testioasys.data.model.Enterprise

class EnterpriseDetailsViewModel : ViewModel() {

    var enterprise: Enterprise? = null
}