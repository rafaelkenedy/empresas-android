package com.rafael.testioasys.data.repository.auth

import com.rafael.testioasys.data.model.UserAuthData

interface LocalAuthProvider {

    fun saveAuthData(userAuthData: UserAuthData)

    fun loadAuthData(): UserAuthData?
}