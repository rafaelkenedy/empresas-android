package com.rafael.testioasys.data.repository.enterprise

import com.rafael.testioasys.data.model.Enterprise

interface EnterpriseRepository {

    suspend fun search(query: String): List<Enterprise>
}