package com.rafael.testioasys.data.api.auth

import com.rafael.testioasys.data.model.AuthBodyRequest
import com.rafael.testioasys.data.model.AuthResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface UserAuthService {

    @POST("users/auth/sign_in")
    suspend fun signIn(@Body bodyRequest: AuthBodyRequest): Response<AuthResponse>
}