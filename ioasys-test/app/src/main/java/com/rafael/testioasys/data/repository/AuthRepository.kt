package com.rafael.testioasys.data.repository

import com.rafael.testioasys.data.model.UserAuthData


interface AuthRepository {

    suspend fun auth(email: String, password: String): UserAuthData

    suspend fun saveAuthData(userAuthData: UserAuthData)

    fun loadAuthData(): UserAuthData?
}