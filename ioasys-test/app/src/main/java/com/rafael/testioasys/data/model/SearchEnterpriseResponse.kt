package com.rafael.testioasys.data.model

data class SearchEnterpriseResponse(val enterprises: List<Enterprise>, val errors: List<String>?)
