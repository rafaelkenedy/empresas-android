package com.rafael.testioasys.data.dao

import com.rafael.testioasys.data.model.UserAuthData

interface UserAuthDAO {

    fun getUserAuthData(): UserAuthData?

    fun saveUserAuthData(userAuthData: UserAuthData)}