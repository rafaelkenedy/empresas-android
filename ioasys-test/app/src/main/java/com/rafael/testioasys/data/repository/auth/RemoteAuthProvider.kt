package com.rafael.testioasys.data.repository.auth

import com.rafael.testioasys.data.model.UserAuthData

interface RemoteAuthProvider {

    suspend fun auth(email: String, password: String): UserAuthData
}