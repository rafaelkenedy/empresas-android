package com.rafael.testioasys.data.model

data class AuthResponse(val success: Boolean, val errors: List<String>?)
