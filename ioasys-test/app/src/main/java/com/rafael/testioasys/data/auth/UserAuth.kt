package com.rafael.testioasys.data.auth

import com.rafael.testioasys.data.model.UserAuthData

interface UserAuth {

    suspend fun doAuth(email: String, password: String): UserAuthData
}